package json.serialization;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import java.io.*;

/**
 * @author Artyom Fyodorov
 */
class JsonSerializer {

    byte[] serialize(JsonObject object) throws IOException {
        try (OutputStream out = new ByteArrayOutputStream();
             JsonWriter jsonWriter = Json.createWriter(out)) {
            jsonWriter.writeObject(object);
            return ((ByteArrayOutputStream) out).toByteArray();
        }
    }

    JsonObject deserialize(byte[] data) throws IOException {
        try (InputStream is = new ByteArrayInputStream(data)) {
            JsonReader jsonReader = Json.createReader(is);
            return jsonReader.readObject();
        }
    }
}