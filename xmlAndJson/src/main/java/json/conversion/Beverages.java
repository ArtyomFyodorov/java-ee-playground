package json.conversion;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;

/**
 * @author Artyom Fyodorov
 */
class Beverages {

    static JsonObject toJson(Beverage beverage) {
        return Json.createObjectBuilder()
                .add("name", beverage.getName())
                .add("size", beverage.getSize())
                .add("price", beverage.getPrice())
                .build();
    }

    public JsonArray allAsJson() {
        Collector<JsonObject, JsonArrayBuilder, JsonArrayBuilder> jsonCollector =
                Collector.of(Json::createArrayBuilder, JsonArrayBuilder::add,
                        (left, right) -> {
                            left.add(right);
                            return left;
                        });

        return all().stream().map(Beverages::toJson)
                .collect(jsonCollector).build();
    }

    public List<Beverage> all() {
        return Arrays.asList(
                Beverage.of("Caffe Mocha", Beverage.Size.SMALL, 3.45),
                Beverage.of("Caffe Mocha", Beverage.Size.MEDIUM, 4.15),
                Beverage.of("Caffe Mocha", Beverage.Size.LARGE, 4.65));
    }
}
