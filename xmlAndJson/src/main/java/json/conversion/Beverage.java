package json.conversion;

/**
 * @author Artyom Fyodorov
 */
class Beverage {
    private String name;
    private Size size;
    private double price;

    private Beverage(String name, Size size, double price) {
        this.name = name;
        this.size = size;
        this.price = price;
    }

    public static Beverage of(String name, Size size, double price) {
        return new Beverage(name, size, price);
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size.name;
    }

    public double getPrice() {
        return price;
    }

    enum Size {
        SMALL("Tall"), MEDIUM("Grande"), LARGE("Venti"), EXTRA_LARGE("Trenta");

        private final String name;

        Size(String name) {
            this.name = name;
        }
    }
}
