package json.combine;

import javax.json.JsonObject;

/**
 * @author Artyom Fyodorov
 */
public interface JsonObjectCombiner {

    JsonObject merge(JsonObject... objects);
}
