package jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artyom Fyodorov
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Developer {
    private String name;
    private Set<String> languages;

    public Developer() {
    }

    public Developer(String name) {
        this.name = name;
        this.languages = new HashSet<>();
    }

    public void add(String language) {
        this.languages.add(language);
    }

    public Set<String> getLanguages() {
        return languages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Developer other = (Developer) o;

        return (name != null
                ? name.equals(other.name)
                : other.name == null)
                && (languages != null
                ? languages.equals(other.languages)
                : other.languages == null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (languages != null ? languages.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + name + '\'' +
                ", languages=" + languages +
                '}';
    }
}
