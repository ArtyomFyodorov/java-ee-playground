package jaxb;

import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class DeveloperTest {

    private JAXBContext context;
    private File file;

    @Before
    public void init() throws Exception {
        this.context = JAXBContext.newInstance(Developer.class);
        this.file = new File("src/main/resources/developer.xml");
    }

    @Test
    public void reserialize() throws Exception {
        Developer polyglot = new Developer("duke");
        polyglot.add("Java");
        polyglot.add("Scala");
        polyglot.add("Kotlin");
        polyglot.add("JavaScript");

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(polyglot, file);

        Unmarshaller unmarshaller = this.context.createUnmarshaller();
        Developer developer = (Developer) unmarshaller.unmarshal(file);

        assertNotSame(polyglot, developer);
        assertThat(developer, is(polyglot));
    }

}