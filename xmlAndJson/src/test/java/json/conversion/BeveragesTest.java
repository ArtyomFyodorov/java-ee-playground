package json.conversion;

import org.junit.Test;

import javax.json.JsonArray;
import javax.json.JsonObject;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class BeveragesTest {

    private Beverages beverages = new Beverages();

    @Test
    public void allAsJsonTest() throws Exception {
        JsonArray jsonArray = beverages.allAsJson();
        assertThat(jsonArray, is(notNullValue()));
        assertThat(jsonArray.size(), is(3));
    }

    @Test
    public void toJsonTest() throws Exception {
        Beverage beverage = Beverage.of("Caffe Latte", Beverage.Size.SMALL, 2.95);
        JsonObject actual = Beverages.toJson(beverage);

        assertThat(actual, is(notNullValue()));
        assertThat(beverage.getName(), is(actual.getString("name")));
        assertThat(beverage.getSize(), is(actual.getString("size")));
        assertThat(beverage.getPrice(), is(actual.getJsonNumber("price").doubleValue()));
    }

}