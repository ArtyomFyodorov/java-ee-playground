package json.combine;

import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class JsonObjectCombinerTest {

    @Test
    public void merge() throws Exception {
        JsonObject devObject = Json.createObjectBuilder()
                .add("developer", "junior")
                .build();
        JsonObject langObject = Json.createObjectBuilder()
                .add("language", "java")
                .build();

        JsonObjectBuilder builder = Json.createObjectBuilder();
        devObject.forEach(builder::add);
        langObject.forEach(builder::add);
        JsonObject mergedObject = builder.build();

        assertThat(mergedObject.getString("developer"), is("junior"));
        assertThat(mergedObject.getString("language"), is("java"));
    }
}