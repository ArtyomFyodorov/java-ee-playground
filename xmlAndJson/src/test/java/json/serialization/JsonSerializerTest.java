package json.serialization;

import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class JsonSerializerTest {

    JsonSerializer js = new JsonSerializer();

    @Test
    public void reserialize() throws Exception {
        JsonObject expected = Json.createObjectBuilder()
                .add("language", "Java")
                .build();

        byte[] raw = js.serialize(expected);
        JsonObject actual = js.deserialize(raw);

        assertNotSame(expected, actual);
        assertThat(actual, is(expected));
    }


}