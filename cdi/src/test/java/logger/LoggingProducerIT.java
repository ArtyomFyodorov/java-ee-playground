package logger;

import basic.AbstractArquillianTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.logging.Logger;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;


/**
 * @author Artyom Fyodorov
 */
public class LoggingProducerIT extends AbstractArquillianTest {

    @Inject Logger logger;

    @Test
    public void injectionTest() throws Exception {
        assertThat(logger, is(notNullValue()));
        assertThat(this.getClass().getName(), is(logger.getName()));
    }
}