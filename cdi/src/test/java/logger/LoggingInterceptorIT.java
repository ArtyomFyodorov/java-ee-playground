package logger;

import basic.AbstractArquillianTest;
import org.junit.Test;

import javax.inject.Inject;

/**
 * @author Artyom Fyodorov
 */
public class LoggingInterceptorIT extends AbstractArquillianTest {

    @Inject Message message;

    @Test
    public void interceptorTest() throws Exception {
        message.greet();
    }
}