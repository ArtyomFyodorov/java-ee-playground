package basic;

import logger.Loggable;
import logger.LoggingInterceptor;
import logger.LoggingProducer;
import logger.Message;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;

/**
 * @author Artyom Fyodorov
 */
@RunWith(Arquillian.class)
public class AbstractArquillianTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Loggable.class)
                .addClass(LoggingProducer.class)
                .addClass(LoggingInterceptor.class)
                .addClass(Message.class)
                .addAsManifestResource("beans_test.xml", "beans.xml");
    }
}
