package logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
@Loggable
@Interceptor
public class LoggingInterceptor {

    private @Inject Logger logger;

    @AroundInvoke
    public Object expose(InvocationContext ic) throws Exception {
//        logger.entering(ic.getTarget().toString(), ic.getMethod().getName());
        logger.info(String.format("ENTRY: %s.%s", ic.getTarget().toString(), ic.getMethod().getName()));
        try {
            return ic.proceed();
        } finally {
//            logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
            logger.info(String.format("RETURN: %s.%s", ic.getTarget().toString(), ic.getMethod().getName()));
        }
    }

}
