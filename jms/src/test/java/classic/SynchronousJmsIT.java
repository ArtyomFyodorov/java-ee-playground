package classic;

import mdb.ConsumerMDBAsync;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import resources.Resources;
import simplified.*;

import javax.ejb.EJB;
import java.time.LocalDateTime;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(Arquillian.class)
public class SynchronousJmsIT {

    private @EJB ClassicProducer classicProducer;
    private @EJB ClassicConsumer classicConsumer;
    private @EJB SimplifiedProducer simplifiedProducer;
    private @EJB SimplifiedConsumer simplifiedConsumer;
    private @EJB SimplifiedProducerCDI simplifiedProducerCDI;
    private @EJB SimplifiedConsumerCDI simplifiedConsumerCDI;
    private @EJB SimplifiedProducerAsync simplifiedProducerAsync;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(
                        Resources.class,
                        ClassicProducer.class,
                        ClassicConsumer.class,
                        SimplifiedProducer.class,
                        SimplifiedConsumer.class,
                        SimplifiedProducerCDI.class,
                        SimplifiedConsumerCDI.class,
                        SimplifiedProducerAsync.class,
                        ConsumerMDBAsync.class
                );
    }


    @Test
    public void testClassicAPI() throws Exception {
        final String expected = String.format("JMS 1.1 - Text message sent at %s\n", LocalDateTime.now());
        classicProducer.sendTextMessage(expected);

        String actual = classicConsumer.receiveTextMessage();
        System.out.println(actual);
        assertThat(actual, is(expected));
    }

    @Test
    public void testSimplifiedAPI_AppManagedJmsContext() throws Exception {
        final String expected = String.format("JMS 2.0 - The text message over app-managed JMSContext sent at %s\n", LocalDateTime.now());
        simplifiedProducer.sendTextMessage(expected);

        String actual = simplifiedConsumer.receiveTextMessage();
        System.out.println(actual);
        assertThat(actual, is(expected));
    }

    @Test
    public void testSimplifiedAPI_ContainerManagedJMSContext() throws Exception {
        final String expected = String.format("JMS 2.0 - The text message over container-managed JMSContext sent at %s\n", LocalDateTime.now());
        simplifiedProducerCDI.sendTextMessage(expected);

        String actual = simplifiedConsumerCDI.receiveTextMessage();
        System.out.println(actual);
        assertThat(actual, is(expected));
    }

    @Test
    public void testMDB() throws Exception {
        final String expected = "MDB - The text message";
        simplifiedProducerAsync.sendTextMessage(expected);

        String actual = simplifiedConsumerCDI.receiveTextMessage();
        System.out.println(actual);
        assertThat(actual, endsWith(expected));
    }
}