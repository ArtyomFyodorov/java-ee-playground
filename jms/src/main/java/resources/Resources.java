package resources;

import javax.jms.JMSConnectionFactoryDefinition;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;

@JMSConnectionFactoryDefinition(
        name = Resources.DEF_CONNECTION_FACTORY,
        className = "javax.jms.ConnectionFactory",
        description = "Default Connection Factory")
@JMSDestinationDefinitions({
        @JMSDestinationDefinition(
                name = Resources.CLASSIC_QUEUE,
                interfaceName = "javax.jms.Queue",
                destinationName = "classicQueue",
                description = "Synchronous Queue"),
        @JMSDestinationDefinition(name = Resources.CONTAINER_MANAGED_AT_RESOURCE_QUEUE,
                interfaceName = "javax.jms.Queue",
                destinationName = "syncAppQueue",
                description = "Synchronous Queue using @Resource JMSContext"),
        @JMSDestinationDefinition(name = Resources.CONTAINER_MANAGED_AT_INJECT_QUEUE,
                interfaceName = "javax.jms.Queue",
                destinationName = "syncContainerQueue",
                description = "Synchronous Queue using @Inject JMSContext"),
        @JMSDestinationDefinition(name = Resources.ASYNC_QUEUE,
                interfaceName = "javax.jms.Queue",
                destinationName = "asyncQueue",
                description = "Asynchronous Queue"),
        @JMSDestinationDefinition(name = Resources.ASYNC_TOPIC,
                interfaceName = "javax.jms.Topic",
                destinationName = "asyncTopic",
                description = "Asynchronous Topic")
})
public class Resources {
    public static final String DEF_CONNECTION_FACTORY = "java:global/jms/defJMSConnectionFactory";
    public static final String CLASSIC_QUEUE = "java:global/jms/classicQueue";
    public static final String CONTAINER_MANAGED_AT_RESOURCE_QUEUE = "java:global/jms/atResourceQueue";
    public static final String CONTAINER_MANAGED_AT_INJECT_QUEUE = "java:global/jms/atInjectQueue";
    public static final String ASYNC_QUEUE = "java:global/jms/asyncQueue";
    public static final String ASYNC_TOPIC = "java:global/jms/asyncTopic";
}
