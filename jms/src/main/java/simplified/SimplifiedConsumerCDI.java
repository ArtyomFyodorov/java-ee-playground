package simplified;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class SimplifiedConsumerCDI {

    @Inject
    @JMSConnectionFactory(Resources.DEF_CONNECTION_FACTORY)
    private JMSContext context;

    @Resource(mappedName = Resources.CONTAINER_MANAGED_AT_INJECT_QUEUE)
    private Queue queue;

    public String receiveTextMessage() {
        try {
            return context.createConsumer(queue).receiveBody(String.class, 5_000);
        } finally {
            System.out.println("\nReceived");
        }

        /*String textMessage = null;
        try {
            Message message = context.createConsumer(queue).receive();
            textMessage = message.getBody(String.class);
            message.acknowledge();
        } catch (JMSException e) {
            Logger.getLogger(SimplifiedConsumerCDI.class.getName()).log(Level.SEVERE, null, e);
        }

        return textMessage;*/
    }

}
