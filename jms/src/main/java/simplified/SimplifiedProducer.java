package simplified;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class SimplifiedProducer {

    @Resource(lookup = Resources.DEF_CONNECTION_FACTORY)
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = Resources.CONTAINER_MANAGED_AT_RESOURCE_QUEUE)
    private Queue queue;

    public void sendTextMessage(String message) {
        try (JMSContext context = connectionFactory.createContext()) {
            context.createProducer().send(queue, message);
            System.out.println("\nSent");
        }
    }
}
