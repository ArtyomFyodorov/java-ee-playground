package simplified;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class SimplifiedProducerAsync {
    private static final Logger log = Logger.getLogger(SimplifiedProducerAsync.class.getName());

    @Inject
    @JMSConnectionFactory(Resources.DEF_CONNECTION_FACTORY)
    private JMSContext context;

    @Resource(mappedName = Resources.ASYNC_TOPIC)
    private Topic asyncTopic;

    public void sendTextMessage(String message) {
        JMSProducer producer = context.createProducer();
        try {
            producer.setAsync(new CompletionListener() {
                @Override
                public void onCompletion(Message msg) {
                    try {
                        System.out.println(msg.getBody(String.class));
                    } catch (JMSException ex) {
                        log.log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void onException(Message msg, Exception e) {
                    try {
                        System.out.println(msg.getBody(String.class));
                    } catch (JMSException ex) {
                        log.log(Level.SEVERE, null, ex);
                    }
                }
            });
        } catch (RuntimeException e) {
            log.info("Caught RuntimeException trying to invoke setAsync - not permitted in Java EE");
        }
        producer.send(asyncTopic, message);
        System.out.println("\nSent");
    }

}
