package simplified;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class SimplifiedConsumer {

    @Resource(lookup = Resources.DEF_CONNECTION_FACTORY)
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = Resources.CONTAINER_MANAGED_AT_RESOURCE_QUEUE)
    private Queue queue;

    public String receiveTextMessage() {
        try (JMSContext context = connectionFactory.createContext()) {
            return context.createConsumer(queue).receiveBody(String.class, 5_000);
        } finally {
            System.out.println("\nReceived");
        }
    }
}
