package simplified;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class SimplifiedProducerCDI {

    @Inject
//    @JMSSessionMode(JMSContext.CLIENT_ACKNOWLEDGE)
    @JMSConnectionFactory(Resources.DEF_CONNECTION_FACTORY)
    private JMSContext context;

    @Resource(mappedName = Resources.CONTAINER_MANAGED_AT_INJECT_QUEUE)
    private Queue queue;

    public void sendTextMessage(String message) {
        context.createProducer().send(queue, message);
        System.out.println("\nSent");
    }
}
