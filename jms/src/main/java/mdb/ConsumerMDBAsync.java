package mdb;

import resources.Resources;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup",
                propertyValue = Resources.ASYNC_TOPIC),
        @ActivationConfigProperty(propertyName = "destinationType",
                propertyValue = "javax.jms.Topic")
})
public class ConsumerMDBAsync implements MessageListener {

    @Inject
    @JMSConnectionFactory(Resources.DEF_CONNECTION_FACTORY)
    private JMSContext context;

    @Resource(mappedName = Resources.CONTAINER_MANAGED_AT_INJECT_QUEUE)
    private javax.jms.Queue syncQueue;

    @Override
    public void onMessage(Message message) {
        try {
            String textMessage = message.getBody(String.class);
            System.out.printf("Message received (async) at %s: %s\n", LocalDateTime.now(), textMessage);
            sendMessage(textMessage);
        } catch (JMSException e) {
            Logger.getLogger(ConsumerMDBAsync.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void sendMessage(String message) {
        String newMessage = String.format("Message has been received and sent again at %s:\n\t%s", LocalDateTime.now(), message);
        context.createProducer().send(syncQueue, newMessage);
    }
}
