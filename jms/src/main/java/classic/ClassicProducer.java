package classic;

import resources.Resources;
import simplified.SimplifiedProducerAsync;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class ClassicProducer {
    private static final Logger log = Logger.getLogger(SimplifiedProducerAsync.class.getName());

    @Resource(lookup = Resources.DEF_CONNECTION_FACTORY)
    private ConnectionFactory connectionFactory;
    @Resource(mappedName = Resources.CLASSIC_QUEUE)
    private Queue queue;

    public void sendTextMessage(String message) {
        try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer msgProducer = session.createProducer(queue);

            TextMessage textMessage = session.createTextMessage(message);
            msgProducer.send(textMessage);
            System.out.println("\nSent");
        } catch (JMSException e) {
            log.log(Level.SEVERE, null, e);
        }
    }
}
