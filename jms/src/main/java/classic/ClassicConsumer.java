package classic;

import resources.Resources;
import simplified.SimplifiedProducerAsync;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class ClassicConsumer {
    private static final Logger log = Logger.getLogger(SimplifiedProducerAsync.class.getName());

    @Resource(lookup = Resources.DEF_CONNECTION_FACTORY)
    private ConnectionFactory connectionFactory;
    @Resource(mappedName = Resources.CLASSIC_QUEUE)
    private Queue queue;

    public String receiveTextMessage() {
        String response = null;
        try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            Session session = connection.createSession();
            MessageConsumer msgConsumer = session.createConsumer(queue);
            Message message = msgConsumer.receive(5_000);
            response = message.getBody(String.class);
            System.out.println("\nReceived");
        } catch (JMSException e) {
            log.log(Level.SEVERE, null, e);
        }

        return response;
    }
}
